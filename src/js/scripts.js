$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true
    });
    $('#menuMobile').slideReveal({
        trigger: $("#triggerMenu"),
        position: 'right'
    });
});

// Add our dependencies
var gulp = require('gulp'), // Main Gulp module
    concat = require('gulp-concat'), // Gulp File concatenation plugin
    open = require('gulp-open'), // Gulp browser opening plugin
    connect = require('gulp-connect'), // Gulp Web server runner plugin
    copy = require('gulp-copy'), // Gulp copy
    sass = require('gulp-sass');
    sass.compiler = require('node-sass');

// Configuration
var configuration = {
    paths: {
        src: {
            html: './*.html',
            assets: './src/assets/*.*',
            js: [
                './src/js/*'
            ],
            css: [
                './src/css/*'
            ],
            scss: [
                './src/scss/*.scss'
            ]
        },
        dist: './dist'
    },
    localServer: {
        port: 8001,
        url: 'http://localhost:8001/'
    }
};

// Gulp task to copy HTML files to output directory
gulp.task('html', function() {
    gulp.src([configuration.paths.src.html])
        .pipe(copy(configuration.paths.dist, { prefix: 1 }))
        .pipe(connect.reload());
});

gulp.task('scss', function () {
    gulp.src(configuration.paths.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
});

gulp.task('css', function () {
    gulp.src(configuration.paths.src.css)
        .pipe(copy(configuration.paths.dist, { prefix: 1 }))
        .pipe(connect.reload());
});

gulp.task('assets', function () {
    gulp.src(configuration.paths.src.assets)
        .pipe(copy(configuration.paths.dist, { prefix: 1 }))
        .pipe(connect.reload());
});

gulp.task('js', function () {
    gulp.src(configuration.paths.src.js)
        .pipe(copy(configuration.paths.dist, { prefix: 1 }))
        .pipe(connect.reload());
});

// Gulp task to create a web server
gulp.task('connect', function () {
    connect.server({
        root: 'dist',
        port: configuration.localServer.port,
        livereload: true
    });
});

// Gulp task to open the default web browser
gulp.task('open', function(){
    gulp.src('dist/index.html')
        .pipe(open({uri: configuration.localServer.url}));
});

// Watch the file system and reload the website automatically
gulp.task('watch', function () {
    gulp.watch(configuration.paths.src.html, ['html']);
    gulp.watch(configuration.paths.src.js, ['js']);
    gulp.watch(configuration.paths.src.scss, ['scss', 'css', 'assets']);
});

// Gulp default task
gulp.task('default', ['html', 'scss', 'css', 'assets', 'js', 'connect', 'open', 'watch']);

